var http = require('http')
var PORT = 3000
var serverUrl = 'localhost'
var fs = require('fs')
var path = require('path')
var bodyParser = require('body-parser')
var express = require('express')
var app = express()
const public = path.join(__dirname) + "/public";
const DEFAULT_SIZE = 3;
var flightList = require("./flight.json");

app.use("/", express.static(public));
// app.use('/', express.static(path.join(__dirname,'public')))
// app.get('/flightSearch',  (req, res) =>{
//   res.sendFile("/assign1.html")
// });

app.get("/", (req, res) => {
	res.sendFile(`${public}/assign1.html`)
});

app.get('/flightSearch',  (req, res) =>{
  res.sendFile(`${public}/assign1.html`)
});
app.get('/displayFlights',  (req, res) =>{
  res.sendFile(`${public}/listflight.html`)
});



const DEFAULT_ITEMS_PER_PAGE = 4;
const DEFAULT_PAGE_NUMBER = 1;

app.get('/flight',(req,res)=>{
  const itemsPerPage = parseInt(req.query.itemsPerPage) || 4;
  const pageNumber = parseInt(req.query.pageNumber) || 1;
  const offset = (pageNumber - 1) * itemsPerPage;

  res.json({
		statusCode: 200,
		flights: flightList.slice(offset, offset + itemsPerPage),
		pageNumber,
		itemsPerPage,
		flightsPageTotal: Math.ceil(flightList.length / itemsPerPage)
	});
});

app.listen(PORT,() =>{
  console.log('start server');
});

// var server = http.createServer(function (req, res) {
//   if (req.url === '/flightSearch') {
//   	fs.readFile('./assign1.html', function (err, text) {
//       res.setHeader('Content-Type', 'text/html')
//       res.end(text)
//     })
//   } else if (req.url === '/displayFlights') {
//     fs.readFile('./listflight.html', function (err, text) {
//       res.setHeader('Content-Type', 'text/html')

//       res.end(text)
//     })
//   } else if (req.url === '/search') {
//     // JSON.stringify(
//     // )
//   } else {
//   	res.end('<p>Only /flightSearch could be open</p>')
//   }
// })

// console.log('Listening at ' + serverUrl + ':' + port)
// server.listen(port, serverUrl)