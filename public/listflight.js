const flightsBadge = $("#page-badge");
const flightsPageTotal = $("#total-badge");

const flightsTable = $("#table-list2");

const flightsPagination = $("#flights-pagination");

let currentPageSelection = 1;
let maximumPageSelection = 1;

function renderFlightsRow(flights) {
	flightsTable.html("");

	flights.forEach(flight => {
		flightsTable.append(`<tr>
			<td style="font-size:20px; color:blue;">${flight.Price}</td>
			<td>${flight.Airline}</td>
			<td>${flight.Takeoff}</td>
			<td>${flight.Landing}</td>
			<td>${flight.Stop}</td>
		</tr>`);
	});
}

function prevPagination() {
	if (currentPageSelection > 1) fetchFlights(--currentPageSelection);
}
function nextPagination() {
	if (currentPageSelection < maximumPageSelection)
		fetchFlights(++currentPageSelection);
}
function renderFlightsPagination(flightsPageTotal) {
	flightsPagination.html("");

	flightsPagination.append(
		`<li class="page-item">
			<a class="page-link" href="#" onclick="prevPagination()">Previous</a>
		</li>`
	);
	for (let i = 0; i < flightsPageTotal; i++) {
		flightsPagination.append(
		`<li  id="page-link-${i +1}" class="page-item">		
		<a class="page-link" href="#" onclick="fetchFlights(${i + 1})">${i +1}</a> </li>`
		
		);
	}
	flightsPagination.append(
		`<li class="page-item">
			<a class="page-link" href="#" onclick="nextPagination()">Next</a>
		</li>`
	);
}

function fetchFlights(pageNumber = 1, itemsPerPage) {
	let query;
	if (!pageNumber && !itemsPerPage) query = "";
	else if (pageNumber && itemsPerPage)
		query = `pageNumber=${pageNumber}&itemsPerPage=${itemsPerPage}`;
	else if (pageNumber) query = `pageNumber=${pageNumber}`;
	else if (itemsPerPage) query = `itemsPerPage=${itemsPerPage}`;

	$.get(`/flight?${query}`, data => {
		maximumPageSelection = data.flightsPageTotal;

		renderFlightsRow(data.flights);
		//Header table navigation
		flightsBadge.html(data.pageNumber);
		flightsPageTotal.html(data.flightsPageTotal);

		//Bottom table navigation
		renderFlightsPagination(data.flightsPageTotal);
		$(`#page-link-${currentPageSelection}`).removeClass(" active");
		$(`#page-link-${pageNumber}`).addClass(" active");
		currentPaxfyrsgeSelection = pageNumber;
	});
}

$(document).ready(() => {
	fetchFlights();
});